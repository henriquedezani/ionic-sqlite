import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = null;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, sqlite: SQLite)
  {
    platform.ready().then(() =>
    {
      sqlite.create({
        name: 'dbtarefas.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS tarefa (id integer primary key AUTOINCREMENT NOT NULL, nome TEXT)')
          .then(() => {
            console.log('Tabelas criadas com sucesso')
            splashScreen.hide();
            this.rootPage.setRoot(HomePage);
          })
          .catch(e => {
            console.log('Exceção ao criar a tabela (tabelas já criadas): ' + JSON.stringify(e));
            splashScreen.hide();
            this.rootPage = HomePage;
          });
      })
      .catch(e => {
        console.log('Exceção ao criar/conectar ao banco de dados: ' + e)
      });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

    });
  }
}

