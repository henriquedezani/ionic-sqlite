import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Component({
  selector: 'page-create',
  templateUrl: 'create.html',
})
export class CreatePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public sqlite: SQLite) {
  }

  public salvar(form: NgForm)
  {

    let nome: string = form.value.nome;

    this.sqlite.create({
      name: 'dbtarefas.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      db.executeSql('INSERT INTO tarefa (nome) VALUES(?)', [nome])
        .then(() => {
          this.navCtrl.pop();
        })
        .catch((e) => {
          console.log('Exceção ao inserir uma tarefa: ' +  + JSON.stringify(e))
        });
    })
    .catch(e => console.log('Exceção ao conectar ao banco de dados: ' + e));
  }

}
