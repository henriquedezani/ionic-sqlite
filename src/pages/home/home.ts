import { CreatePage } from './../create/create';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public lista: Array<any> = [];

  constructor(public navCtrl: NavController, public sqlite: SQLite) {
  }

  ionViewDidEnter()  {
    this.sqlite.create({
      name: 'dbtarefas.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      db.executeSql('SELECT id, nome FROM tarefa', [])
        .then((result) => {
          this.lista = [];
          if (result.rows.length > 0) {
            for(var i=0; i<result.rows.length; i++) {
              this.lista.push({id: result.rows.item(i).id, nome: result.rows.item(i).nome});
            }
          }
        })
        .catch((e) => {
          console.log('Exceção ao consultar tarefas: ' + JSON.stringify(e));
        });
    })
    .catch((e) =>
      {
        console.log('Exceção ao conectar ao banco de dados: ' + e)
      });
  }

  adicionar() {
    this.navCtrl.push(CreatePage);
  }
}
